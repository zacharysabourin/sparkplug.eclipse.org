---
title: "Get Listed"
headline: "GET LISTED"
header_wrapper_class: 'header-sparkplug-get-listed text-left'
description: "Get listed on Sparkplug"
keywords: ["eclipse", "eclipse Sparkplug", "get listed"]
layout: "single"
container: "container-fluid get-listed-page"
featured_footer_class: get-listed-footer
hide_sidebar: true
hide_page_title: true
---

{{< compatibility/get-listed/steps >}}

{{< compatibility/get-listed/benefits >}}

{{< grid/div class="container get-listed-text-container" >}}
## Download, Run and Pass the TCK
- TCK is open-source and [developed on GitHub](https://github.com/eclipse/sparkplug/tree/master)
- Product Certification Requests: Tests must be run with a binary version of the TCK licensed under the [Eclipse Foundation Technology Compatibility Kit License](https://www.eclipse.org/legal/tck.php)
- TCK binaries will be available on download.eclipse.org
- To claim compatibility, you need to be compliant with the Eclipse Foundation Technology Compatibility Kit License
{{</ grid/div >}}

{{< grid/div class="container get-listed-text-container" >}}
## Become a Member
- Only members of the Eclipse Foundation and its Sparkplug working group can participate in the Sparkplug compatibility program
- Foundation and working group memberships are separate and require the payment of annual fees
  - [Eclipse Foundation membership fees](https://www.eclipse.org/membership/#tab-fees) (need contributing or strategic level)
  - [Sparkplug working group membership fees](https://www.eclipse.org/org/workinggroups/eclipse_sparkplug_charter.php) (any level)
- To get started, fill the online [Membership Application Form](https://membership.eclipse.org/application#sign-in)
- Relevant agreements
  - [Eclipse Foundation Membership Agreement](https://www.eclipse.org/org/documents/eclipse_membership_agreement.pdf)
  - [Member Committer and Contributor Agreement](https://www.eclipse.org/legal/committer_process/EclipseMemberCommitterAgreement.pdf)
  - [Sparkplug Working Group Participation Agreement](https://www.eclipse.org/org/workinggroups/wgpa/sparkplug-working-group-participation-agreement.pdf)
{{</ grid/div >}}

{{< grid/div class="container get-listed-text-container" >}}
## Compatibility Trademark License Agreement 
- The [Sparkplug Compatibility Trademark License Agreement](https://docs.google.com/document/d/1m9ljsjcY7Zo97MLbW5fr8E7YdFj4jyPyzQsG_iGJWZg) governs the use of the "Sparkplug Compatible" wordmark and logo
- Organizations need to execute it to participate in the Sparkplug compatibility program
{{</ grid/div >}}

{{< grid/div class="container get-listed-text-container" >}}
## File a "Get Listed" Request
- Certification requests are managed through GitHub issues
- The Foundation provides an [issue template](https://docs.google.com/document/u/0/d/1NC0Mb8hkKSS8BC2GLjZWn_wacrrvyUvHBHKOU4-q5os/edit)
- Running the TCK produces a report that the filer exposes on the Internet and provides the URL for.
- Certification requests need to be approved by a committer of the Sparkplug specification project
  - Process to be discussed with the team 
- Once approved, Foundation staff adds the product to the list of compatible products on the [Sparkplug website](https://sparkplug.eclipse.org).

{{</ grid/div >}}

{{< grid/div class="container get-listed-text-container margin-bottom-60" >}}
## Promote Your Compatible Product 
- Usage of the "Sparkplug Compatible" wordmark and logo on your website and in marketing material
- Products are listed on the "Compatible Products" page on the Sparkplug website
  - See the [Jakarta EE page](https://jakarta.ee/compatibility/) for an example
  - [Design](https://docs.google.com/document/d/1J9tUvOiBdaGaZswQa_90fgBpeQoLpPYsgbwvfI6qgiA) of the Sparkplug page
- The Foundation publishes a [Sparkplug Brand Usage Handbook](https://docs.google.com/presentation/d/15FfQSmtJnXVwK4qf6KsZo6p4H8uyKroO3grGMiwyw_Y/) that provides guidance to members
{{</ grid/div >}}
