+++
date = "2022-04-07"
title = "Industrial IoT made fast and easy"
link = "https://www.controleng.com/articles/industrial-iot-made-fast-and-easy/"
+++

With the tools available today, implementing the Industrial Internet of Things (IIoT) is not as hard as people think.
