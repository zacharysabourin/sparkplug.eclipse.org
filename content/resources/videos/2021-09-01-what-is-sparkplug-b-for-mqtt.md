+++
date = "2021-09-01"
title = "What is Sparkplug B for MQTT?"
link = "https://www.youtube.com/watch?v=-9vMAe7P25A"
link_class  = "eclipsefdn-video"
tags = [ "video", "open source", "Eclipse", "sparkplug", "MQTT"]
categories = ["video"]
+++

Walker Reynolds explains what Sparkplug B is for MQTT.
