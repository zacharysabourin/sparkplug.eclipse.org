+++
date = "2021-07-23"
title = "What MQTT is why it was invented"
link = "https://cirrus-link.com/videos/mqtt-explained-by-arlen-nipper/"
link_class  = "eclipsefdn-video"
src_site = "others"
video_img = "https://i.vimeocdn.com/video/1196853379-9df889361fd4c457733bb0911d2c26058dcfd916b944b90b7c70f6cefac5045f-d?mw=720"
tags = [ "video", "open source", "Eclipse", "sparkplug", "MQTT"]
categories = ["video"]
+++

In this video from Inductive Automation, Arlen Nipper, the co-inventor of MQTT, explains what MQTT is why it was invented.
