+++
date = "2022-03-24"
title = "Sparkplug Community Meeting"
link = "https://www.crowdcast.io/e/sparkplug-community"
link_class  = "eclipsefdn-video"
src_site = "others"
video_img = "/images/sparkplug/video-covers/sparkplug-community-meeting.jpg"
tags = [ "video", "open source", "Eclipse", "sparkplug", "community"]
categories = ["video"]
+++

The demand for interoperability of industrial systems has never been stronger. 
