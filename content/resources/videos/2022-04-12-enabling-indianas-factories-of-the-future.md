+++
date = "2022-04-12"
title = "Enabling Indiana’s Factories of the Future"
link = "https://cirrus-link.com/videos/enabling-indianas-factories-of-the-future/"
link_class  = "eclipsefdn-video"
src_site = "others"
video_img = "https://i.vimeocdn.com/video/1412832608-370c879e92c8b8c21147c5c5c5288fce5f333390cb228ca993b53726c54e1014-d?mw=720"
tags = [ "video", "open source", "Eclipse", "sparkplug", "Indiana", "Factories"]
categories = ["video"]
+++

Learn about how Indiana is leading the way in advancing manufacturers to make Industry 4.0 quick, simple, and cost-effective.
